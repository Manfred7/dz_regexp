﻿using System;
using System.CodeDom;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace dz
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            string hrefPattern = @"(?<=('|""))https?[\w\W]*?(?=('|""))";
            string sourceUrl = "https://blades.guru";
            string htmlPage;

            try
            {
                htmlPage = ppvTools.GET(sourceUrl);

                MatchCollection matches = Regex.Matches(htmlPage, hrefPattern);
                foreach (Match match in matches)
                    Console.WriteLine("'{0}'", match.Value);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}